using NUnit.Framework;
using test17022020;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            test01 ts = new test01();
            string s = ts.getdata("str");
            Assert.AreEqual("Hello :str", s);
        }
    }
}